describe port(2049) do
  it { should be_listening }
end

# The runners on gitlab.com does not allow us to use mount
# describe command('mount 127.0.0.1:/foo /mnt') do
#   its(:exit_status) { should eq 0 }
# end

# describe file('/mnt/test') do
#   its(:content) { should eq 'success' }
# end

# describe command('umount /mnt') do
#   its(:exit_status) { should eq 0 }
# end
