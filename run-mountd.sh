#!/bin/bash
set -eux
exportfs -va
rpcbind
rpc.statd
rpc.nfsd

# first arg is `-f` or `--some-option`
if [ "${1#-}" != "$1" ]; then
	set -- rpc.mountd "$@"
fi

exec "$@"
