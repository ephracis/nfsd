docker_build = ->
  """
  docker build -t $REGISTRY/$IMAGE \
               --build-arg version=<%= githash.main.tag %> .
  docker tag $REGISTRY/$IMAGE \
             $REGISTRY/$IMAGE:<%= githash.main.tag %>
  """

docker_push = ->
  """
  docker login -u $REGISTRY_USER -p $REGISTRY_PASS $REGISTRY
  docker push $REGISTRY/$IMAGE:latest
  docker push $REGISTRY/$IMAGE:<%= githash.main.tag %>
  """

docker_lint = ->
  """
  dockerfile_lint -f Dockerfile
  """

docker_test = ->
  """
  docker build -t $REGISTRY/$IMAGE:test test
  docker run --rm --privileged $REGISTRY/$IMAGE:test
  """

docker_clean = ->
  """
  docker rmi $REGISTRY/$IMAGE:test
  """

module.exports =
  docker:
    command: (action) ->
      switch action
        when 'build' then docker_build()
        when 'save' then docker_save()
        when 'test' then docker_test()
        when 'push' then docker_push()
        when 'lint' then docker_lint()
        when 'clean' then docker_clean()
