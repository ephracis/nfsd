FROM fedora:25
ARG version
LABEL Name="Jupiter NFS Server" \
      Version="$version"

RUN dnf -y install nfs-utils && \
    dnf clean all
COPY run-mountd.sh /

EXPOSE 2049/tcp
VOLUME ["/exports"]
ENTRYPOINT ["/run-mountd.sh"]
CMD ["--foreground"]
