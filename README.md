# Containerized NFS server

This container runs an NFS server.

## How to use it

Let's say that you want to expose the `/mnt/nas/my-share` folder located on the
server to clients as `my-nas-share`. You need to configure the export file:

    $ cat /etc/exports
    /exports/my-nas-share  *(rw)

Then start the container:

    docker run --cap-add=SYS_ADMIN \
               -d \
               -v /etc/exports:/etc/exports \
               -v /mnt/nas/my-share:/exports/my-nas-share \
               -p 2049:2049 \
               registry.gitlab.com/ephracis/jupiter/nfsserver

Now you can mount it on the clients as `/home/maggie/nas-share:

    sudo mount <SERVER>/my-nas-share /home/maggie/nas-share

## Development

### Requirements

The following needs to be installed on the workstation:

- Docker
- Node
- Ruby

### Get started

Install all packages:

```shell
npm install
```

The system can be built inside a virtualized lab for testing and development.
Grunt is used for running common tasks:

```shell
grunt lint              # lint the code
grunt build             # build the container
grunt test              # run tests against the container
```

### Release

To release a new version use the following commands:

```shell
# assuming that the current release is 0.0.0
grunt release:major     # bumps to 1.0.0
grunt release:minor     # bumps to 0.1.0
grunt release:patch     # bumps to 0.0.1
grunt release:git       # bumps to 0.0.0-ge96c
grunt release:prepatch  # bumps to 0.0.1-rc.0
grunt release:preminor  # bumps to 0.1.0-rc.0
grunt release:premajor  # bumps to 1.0.0-rc.0
grunt release           # just an alias for grunt release:minor
```
