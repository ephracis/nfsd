# Change log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## 0.1.1 - 2017-08-14
### Fixed
- The container can now run in foreground mode. (#1)

## 0.1.0 - 2017-08-13
### New
- NFS server inside a container.

